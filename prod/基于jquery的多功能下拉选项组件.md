## 基于jquery的多功能单选下拉框组件

### 1、前言

单选下拉框是系统开发中常用的组件，也是要求比较多的组件。最近就有一个项目各种改，一会儿要制定规则筛选匹配，一会儿要回车选中，一会儿要懒加载......与其网上各种搜合适的组件，不如自己写一个一劳永逸，更重要是的在功能实现上不受限于其他组件。该组件基于jquery开发，对单选下拉框功能进行整合，能够满足大部分功能需要。后续功能持续更新完善中。

### 2、提供功能

- 支持自定义下拉框右侧图标，图标基于font字体库设计,目前提供了arrow(箭头)和triangle(三角)两种模式 ,可扩展。
- 支持自定义key，value字段名。
- 支持前端动态添加全部选项。
- 支持默认选中。
- 支持输入筛选功能。其中包括是否区分大小写，根据筛选规则（以指定字符串开始匹配、以指定字符串结束匹配、存在即匹配）进行匹配，下拉选项列表展示方式（输入前展示、输入后展示）。
- 支持回车选中。根据回车选中规则(全匹配、模糊匹配)进行匹配。
- 支持数据懒加载。
- 支持通过鼠标上下键控制选项。
- 提供了获取选中key、获取选中value、刷新下拉数据的方法。
- 提供了点击、回车选中时的回调方法。

### 3、参数设置

```js
this.defaultConfig = {
      // 默认父元素
      parentEleClass: "selectSearchArea",
      // 后缀图标，默认arrow；
      // 目前提供arrow(箭头)和triangle(三角)两种模式，可根据项目情况自行拓展
      // 拓展可以添加对应的font，提供一对class icon-${suffixIcon}-top && icon-${suffixIcon}-bottom
      suffixIcon: "arrow",
      // 下拉数组,用于下拉选项的显示
      selectList: [],
      // 下拉选项key值字段, 默认key
      keyField: "key",
      // 下拉选项value值字段, 默认value
      valueField: "value",
      // 输入查询相关配置
      searchSupportOption: {
        // 是否支持输入查询，默认支持，默认值true
        support: true,
        // 是否区分大小写进行匹配, 默认区分，默认值true
        sensitive: true,
        // 匹配方式
        // start: 检测字符串是否以指定的子字符串开始
        // end：检测字符串是否以指定的子字符串结束
        // all: 检索字符串在整个查询中存在
        matchedCondition: "all",
        // 输入为空时是否显示下拉选项
        isViewItemsWhenNoInput: true,
      },
      // 是否需要前端添加全部选项
      isNeedAll: false,
      // 全部选项配置内容
      allOptions: {
        // 全部选项key值，不能为空
        allKeyData: "all",
        // 全部选项value值
        allValueData: "全部",
      },
      defaultSelect: "",
      // 回车相关配置
      enterSelectSupportOption: {
        // 是否支持回车选中， 默认支持，默认值true
        support: true,
        // 支持回车选中情况下定义的选中规则， 默认值 part
        // 目前提供complete(完全匹配) 和 part(部分匹配)两种模式，已存在键盘上、下移键选中
        // complete情况下会根据当前输入文本完全匹配下拉选项的文本，当存在多个选项匹配时，默认选中第一个
        // part情况下会根据当前输入文本模糊匹配，当存在多个选项匹配时，默认选中第一个
        // 优先级：已存在通过键盘上、下键选中的选项 > methods
        methods: "part",
      },
      // 懒加载相关配置
      lazyLoadSupportOption: {
        // 是否支持懒加载, 默认false
        support: false,
        // 下拉数量
        total: 0,
        // 懒加载时调用的方法，由外部定义。需要返回一个Promise()对象，其中resolve()返回的必须是数组。
        lazyloadCallback: () => {},
        // 需要重新刷新数据时的回调方法，由外部定义。需要返回一个Promise()对象，其中resolve()返回的必须是数组
        // 调用该方法时需要重置原引入文件中的js变量(分页等)，返回刷新数据
        reloadCallback: () => {}
      },
    };
```

### 4、提供方法

1. getKey()：获取当前选中选项key值。
2. getValue()：获取当前选中选项的value值。
3. setKey()： 给当前下拉框赋值，只能是存在的key值。
4. refresh(): 重置/刷新/清空列表方法，传入下拉选项数组。
5. setLazyLoadDataTotal()：使用懒加载时给懒加载数量total赋值(一般情况下不需要用到)。
6. change(): 提供了点击、回车选中的回调方法，可用于外部的数据处理、页面刷新等。

### 5、基本使用

1. 引入`jquery`、`/prod/css/multiFunctionSelect.css`、`/prod/font/iconfont.css`,`/prod/js/multiFunctionSelect.min.js`。

```html
<link rel="stylesheet" href="./font/iconfont.css"/>
<link rel="stylesheet" href="./css/multiFunctionSelect.css" />
<script type="text/javascript" src="../lib/jquery/jquery-1.10.0.js"></script>
<body>
  ...
 </dody>
<script type="text/javascript" src="./dist/multiFunctionSelect.min.js"></script>
```

2. 获取dom后调用MultiFunctionSelect传入对应参数即可。

```js
var demo0 = $("#demo0").MultiFunctionSelect({
    selectList: data1,
    keyField: "key",
    valueField: "value",
    searchSupportOption: {
      support: false
    }
});
```

具体用法及实现demo见index.html

### 6、备注

1. 关于懒加载的设计，这里只提供了监听滚动条触底后动态添加dom的基础方法，数据结果完全由外部定义方法获取。

   之前也有考虑过内置ajax然后完全由multiFunctionSelect内部做处理的方式，但是最后否决了，这样做有很多弊端。

   1) 各项目对ajax的配置并非相同，例如请求头设置、参数设置等，并且部分项目返回xml、protobuf等数据格式需要进行二次处理，无法做到通用性。

   2) 避免不必要的数据交互，例如前端分页的实现，一次请求所有数据，懒加载时仅需要前端筛选。

   3) 更重要让使用者能自己进行更多的逻辑处理，不受限于该组件。

   使用上也很简单，使用者仅需提供lazyloadCallback、reloadCallback两个方法以及total值即可。lazyloadCallback用于加载时提供相应数据，reloadCallback会在初始化时获取数据、以及输入时重置数据，并且重置外部定义的变量(例如分页等)。组件期望调用lazyloadCallback、reloadCallback方法后返回值是个Promise，multiFunctionSelect内部已经做了同步处理。

   lazyloadCallback()、reloadCallback() 返回的数据格式为`{ data: [], total: Number }`;

   前端分页实现demo:

   ```js
   var dzdthList；
   function initSelectData = async function() {
     dzdthList = await getAllData();
     var demo = $("#demo").MultiFunctionSelect({
               lazyLoadSupportOption: {
                   support: true,
                   total: 0,
                   lazyloadCallback: lazyloadCallback,
                   reloadCallback: reloadCallback,
               }
           })
     lazyloadOptions.total = dzdthList.length;
   }  
   // 配置懒加载使用到的相关变量
   var lazyloadOptions = {
     total: 0,
     pageSize: 50,
     pageNum: 1,
     curLoadData: []
   }
   
   // 触底加载方法
   function lazyloadCallback() {
     return new Promise(function(resolve) {
       lazyloadOptions.pageNum ++;
       var itemsArr = getSliceData(lazyloadOptions.curLoadData,(lazyloadOptions.pageNum - 1) * lazyloadOptions.pageSize ,  lazyloadOptions.pageNum * lazyloadOptions.pageSize);
       resolve({ data:itemsArr, total: lazyloadOptions.total })
     })
   }
   
   // 懒加载reload方法
   function reloadCallback(innerText) {
     return new Promise(function(resolve) {
       lazyloadOptions.pageNum = 1;
       lazyloadOptions.curLoadData = innerText ? dzdthList.filter(item => item.ZBCJ.toLowerCase().indexOf(innerText.toLowerCase()) > -1) : dzdthList;
       lazyloadOptions.total = lazyloadOptions.curLoadData.length;
       var itemsArr = getSliceData(lazyloadOptions.curLoadData,0,  lazyloadOptions.pageSize);
       resolve({ data:itemsArr, total: lazyloadOptions.total })
     })
   }
   
   // 数组分割方法
   function getSliceData(arr, startIndex, endIndex) {
     return arr.slice(startIndex, endIndex)
   }
   ```

2. 浏览器兼容上，已经适配了主流浏览器，例如谷歌、safari、360、火狐、IE10+等。

3. 如果浏览器提示`$(...).MultiFunctionSelect() is not a function` 错误，请确认当前的$是否是已经添加方法的对象。

4. 如有问题或者建议请联系， 邮箱：hviker2021@126.com

5. 源码地址: https://gitee.com/hxxqwer/multi-function-select