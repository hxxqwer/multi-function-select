const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const Webpack = require("webpack");

module.exports = {
    entry: {
        multiFunctionSelect: "./js/multiFunctionSelect.js",
    },
    plugins: [
        new CleanWebpackPlugin(),  
    ],
    externals: {
        jquery: 'jQuery'
    },
    module: {
        rules: [
            {
                test:/\.js$/,//正则匹配所有以.js结尾的文件
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: [
                                '@babel/plugin-transform-runtime'
                            ]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
}