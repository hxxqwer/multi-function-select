/**
 * Author: hviker
 * Description: 多功能下拉选项
 * Time: 20210602
 */
import $ from 'jquery';
import "babel-polyfill";
var MultiFunctionSelect = function (ele, opts) {
      this.$ele = $(ele);
      this.opts = opts;
      this.closeFlag = true; // 下拉选项关闭标识， true已关闭， false 打开
      this.$matchedList = []; // 当前符合查询条件的下拉数据
      this.$curSelect = null; // 当前选中内容
      this.$curSelectReal = null; // 临时当前选中内容
      this.timeout = null; // 监听input输入查询事件
      this.scrollTimeout = null; // 监听滚动条滚动事件
      this.defaultConfig = {
          // 默认父元素
          parentEleClass: "selectSearchArea",
          // 后缀图标，默认arrow；
          // 目前提供arrow(箭头)和triangle(三角)两种模式，可根据项目情况自行拓展
          // 拓展可以添加对应的font，提供一对class icon-${suffixIcon}-top && icon-${suffixIcon}-bottom
          suffixIcon: "arrow",
          // 下拉数组,用于下拉选项的显示
          selectList: [],
          // 下拉选项key值字段, 默认key
          keyField: "key",
          // 下拉选项value值字段, 默认value
          valueField: "value",
          // 输入查询相关配置
          searchSupportOption: {
              // 是否支持输入查询，默认支持，默认值true
              support: true,
              // 是否区分大小写进行匹配, 默认区分，默认值true
              sensitive: true,
              // 匹配方式
              // start: 检测字符串是否以指定的子字符串开始
              // end：检测字符串是否以指定的子字符串结束
              // all: 检索字符串在整个查询中存在
              matchedCondition: "all",
              // 输入为空时是否显示下拉选项
              isViewItemsWhenNoInput: true,
          },
          // 是否需要前端添加全部选项
          isNeedAll: false,
          // 全部选项配置内容
          allOptions: {
              // 全部选项key值，不能为空
              allKeyData: "all",
              // 全部选项value值
              allValueData: "全部",
          },
          defaultSelect: "",
          // 回车相关配置
          enterSelectSupportOption: {
              // 是否支持回车选中， 默认支持，默认值true
              support: true,
              // 支持回车选中情况下定义的选中规则， 默认值 part
              // 目前提供complete(完全匹配) 和 part(部分匹配)两种模式，已存在键盘上、下移键选中
              // complete情况下会根据当前输入文本完全匹配下拉选项的文本，当存在多个选项匹配时，默认选中第一个
              // part情况下会根据当前输入文本模糊匹配，当存在多个选项匹配时，默认选中第一个
              // 优先级：已存在通过键盘上、下键选中的选项 > methods
              methods: "part",
          },
          // 懒加载相关配置
          // 为了适应各个项目，这里主要提供了监听滚动条触底然后动态添加dom的方法，数据结果完全由外部定义方法获取。
          // 采取使用外部定义方法获取来数据结果的方式主要是基于考虑以下三点：
          // ** 原因一：使用jquery的大部分项目虽然在前后端交互上采用ajax，但是对于ajax的配置却并非相同。例如请求头设置、参数设置，尤其是返回数据为xml、pb等数据格式时需要进行二次处理。而且也不排除少部分项目使用XHR的情况。
          // ** 原因二：避免不必要的数据交互，例如前端分页的实现，一次请求所有数据，懒加载时仅需要前端筛选。
          // ** 原因三：更重要是方便前端开发者自己进行更多的逻辑处理，不受限于该组件。
          lazyLoadSupportOption: {
              // 是否支持懒加载, 默认false
              support: false,
              // 下拉数量
              total: 0,
              // 懒加载时调用的方法，由外部定义。需要返回一个Promise()对象，其中resolve()返回的必须是数组。
              lazyloadCallback: () => {},
              // 需要重新刷新数据时的回调方法，调用该方法时需要重置原引入文件中的js变量(分页等)，返回刷新数据
              reloadCallback: () => {}
          },
          selectFirstWhenNoInput: false, // 当输入空值时是否可以选中第一个
      };
  };

  MultiFunctionSelect.prototype = {
      // 初始化方法
      init: function () {
          var _this = this;
          // 避免selectList出现数据污染
          // 这里不采用对象整体深拷贝的方式是因为懒加载需要调用外部方法，整体深拷贝会使传入方法失效
          _this.opts.selectList = _this.opts.selectList && _this.opts.selectList.length > 0 ? [..._this.opts.selectList]: [];
          _this.$options = mergeOptions(_this.defaultConfig, _this.opts);
          createIconView(_this, _this.$options.suffixIcon);
          // 不支持输入选择情况下添加readonly属性
          !_this.$options.searchSupportOption.support &&
          _this.$ele.attr("readonly", "readonly");
          // 确保selectList为数组
          _this.$options.selectList = isArray(_this.$options.selectList, '下拉数组');
          _this.$matchedList = _this.$options.selectList;
          // 用于前端添加全部选项
          if (_this.$options.isNeedAll && _this.$options.selectList.length > 0) {
              var obj = setSelectItemObj(
                  _this,
                  _this.$options.allOptions.allKeyData,
                  _this.$options.allOptions.allValueData
              );
              _this.$options.selectList.unshift(obj);
          }
          if(_this.$options.defaultSelect) {
              _this.setKey(_this.$options.defaultSelect);
          }
          _this.initAllEvent();
          return _this;
      },

      /**
       * 绑定鼠标滚动事件
       * @param {*} curSelect
       */
      bindScrollEvent: function (curSelect) {
          var _this = this;
          var dom = _this.$ele.parent().find(".multiFunctionSelectList")[0];
          $(dom).scroll(function () {
              var that = this;
              // 这里设置防抖，避免多次计算元素滚动条高度
              if (_this.scrollTimeout) clearTimeout(_this.scrollTimeout);
              _this.scrollTimeout = setTimeout(async function () {
                  // 获取显示区域高度、滚动距离顶部高度、可滚动高度
                  var offsetHeight = $(that).innerHeight(),
                      scrollTop = $(that).scrollTop(),
                      scrollHeight = $(that)[0].scrollHeight;
                  if (offsetHeight + scrollTop >= scrollHeight) {
                      if (
                          _this.$options.selectList.length <
                          _this.$options.lazyLoadSupportOption.total
                      ) {
                          // 添加遮罩层
                          addorDeleteMaskLayer(_this, true);
                          // 请求数据
                          var lazyloadData = await _this.$options.lazyLoadSupportOption.lazyloadCallback();
                          var appendHtml = getSelectListDomStr(
                              _this,
                              lazyloadData.data,
                              curSelect,
                              _this.$options.selectList.length
                          );
                          $(that).append(appendHtml);
                          _this.$options.selectList = [..._this.$options.selectList, ...lazyloadData.data];
                          _this.$matchedList = [..._this.$matchedList, ...lazyloadData.data];
                          addorDeleteMaskLayer(_this, false);
                      } else {
                          addorDeleteMaskLayer(_this, true, true);
                          setTimeout(function () {
                              addorDeleteMaskLayer(_this, false);
                          }, 1000);
                      }
                  }

              }, 500);
          });
      },

      /**
       * 初始化目前存在的操作事件
       */
      initAllEvent: function () {
          var _this = this;
          var suffixIcon = _this.$options.suffixIcon;

          // 输入框focus事件
          _this.$ele.focus(async function (e) {
              _this.closeFlag = true;
              _this.$ele
                  .next()
                  .removeClass("icon-" + suffixIcon + "-bottom")
                  .addClass("icon-" + suffixIcon + "-top");
              // 删除原有的ul
              _this.$ele.parent().find(">ul").remove();
              var innerText = _this.$options.searchSupportOption.support
                  ? _this.$ele.val().trim()
                  : "";
              var matchedSelectList = [];
              var searchSupportOption = _this.$options.searchSupportOption;
              // 1、可输入且输入空时需要显示时
              // 2、不可输入时
              // 3、存在输入内容时
              if (
                  (searchSupportOption.support &&
                      searchSupportOption.isViewItemsWhenNoInput) ||
                  !searchSupportOption.support ||
                  innerText
              ) {
                  // 当不需要懒加载时
                  if(!_this.$options.lazyLoadSupportOption.support) {
                      matchedSelectList = getMatchedSelectData(
                          _this,
                          _this.$options.selectList,
                          innerText
                      );
                  } else {
                      // 这里考虑以下几种情况
                      // ** 1 当$matchedList不存在时，说明是初次加载。需要调用reloadCallback来添加数据。
                      // ** 2 当$matchedList存在时，
                      // **** 2.1 如果是已存在选中数据，则$matchedList为当前选中text对应的数据。这里涉及到匹配规则，但是可以缩减筛选范围，直接筛选$matchedList即可
                      // **** 2.2 如果没有选中数据，考虑过直接使用历史加载下的$matchedList。但是这种情况会存在问题。
                      // **** 例如输入后已经对数据进行筛选，这种情况下$matchedList是不完整的，仅仅是筛选后的数据。
                      // 2.1 的情况在blur事件中已做处理，2.2的情况blur事件中会判断当前有没有选中数据，没有则将$matchedList置为空
                      // focus事件中会判断$matchedList不存在值时重新请求，存在值则直接显示当前的$matchedList
                      if(_this.$matchedList.length === 0) {
                          var { data, total } = await _this.$options.lazyLoadSupportOption.reloadCallback(innerText);
                          _this.$options.selectList = matchedSelectList = data;
                          _this.setLazyLoadDataTotal(total);
                      } else {
                          _this.$options.selectList = matchedSelectList = _this.$matchedList;
                      }
                  }
              }
              _this.$matchedList = matchedSelectList;
              if(!(searchSupportOption.support && !searchSupportOption.isViewItemsWhenNoInput && !innerText)) {
                  createSelectItems(_this, matchedSelectList, _this.$curSelect);
              }
          });

          // 输入框click事件
          _this.$ele.click(function () {
              !_this.closeFlag && _this.$ele.focus();
          });

          // 图标click事件
          _this.$ele.next().click(function(){
              _this.$ele.focus();
          })

          // 输入框blur事件
          _this.$ele.blur(function () {
              if (!_this.closeFlag) return;
              if (!_this.$curSelect) {
                  _this.$ele.attr("data-select", "").val("");
              }
              // 不需要懒加载的情况
              if(!_this.$options.lazyLoadSupportOption.support) {
                  _this.$matchedList = [];
              } else {
                  // 需要懒加载的情况
                  // 如果当前存在已选中的数据，就更新$matchedList为当前text匹配的数据
                  // 不存在时，将$matchedList置为空
                  if(_this.getKey()) {
                      _this.$options.selectList = _this.$matchedList = getMatchedSelectData(_this, _this.$matchedList, _this.getValue());
                  } else {
                      _this.$options.selectList = _this.$matchedList = [];
                  }
              }
              _this.$ele
                  .next()
                  .removeClass("icon-" + suffixIcon + "-top")
                  .addClass("icon-" + suffixIcon + "-bottom");
              _this.$ele.parent().find(">ul").remove();
          });

          // 下拉选项click事件
          _this.$ele.parent().on("mousedown", "li", function () {
              var dataId =
                      $(this).attr("data-id") == "none" ? "" : $(this).attr("data-id"),
                  dataText = $(this).text() == "暂无数据" ? "" : $(this).text();
              _this.$ele
                  .next()
                  .removeClass("icon-" + suffixIcon + "-top")
                  .addClass("icon-" + suffixIcon + "-bottom");
              _this.$ele.attr("data-select", dataId).val(dataText);
              _this.$ele.parent().find(">ul").remove();
              if(!_this.$options.lazyLoadSupportOption.support) {
                  _this.$matchedList = [];
              } else {
                  // 点击时只存在选中情况
                  _this.$options.selectList = _this.$options.selectList = _this.$matchedList = getMatchedSelectData(_this, _this.$matchedList, dataText);
              }
              // 将当前选中内容存储到$curSelect中
              _this.$curSelect = setSelectItemObj(_this, dataId, dataText);
              _this.closeFlag = false;
          });

          // 输入框input事件
          if (_this.$options.searchSupportOption.support) {
              _this.$ele.on("input", function () {
                  // 当输入框内容发生变化时，就清空$curSelect
                  _this.$curSelect = null;
                  _this.$ele
                      .next()
                      .removeClass("icon-" + suffixIcon + "-bottom")
                      .addClass("icon-" + suffixIcon + "-top");
                  _this.$ele.parent().find(">ul").remove();
                  // 设置防抖，避免快速输入时进行连续计算
                  if (_this.timeout) clearTimeout(_this.timeout);
                  _this.timeout = setTimeout(async function () {
                      var innerText = _this.$ele.val().trim();
                      var searchSupportOption = _this.$options.searchSupportOption;
                      var matchedSelectList = [];
                      // 只有可输入时才会绑定input事件，这里就不需要判断是否可输入了
                      // 1、可输入且输入空需要显示时
                      // 2、存在输入内容时
                      if ((searchSupportOption.isViewItemsWhenNoInput) || innerText) {
                          // 当不需要懒加载时
                          if(!_this.$options.lazyLoadSupportOption.support) {
                              matchedSelectList = getMatchedSelectData(
                                  _this,
                                  _this.$options.selectList,
                                  innerText
                              );
                          } else {
                              // 输入后需要筛选数据，所以需要重新对selectList、total、$matchedList进行计算赋值
                              var { data, total } = await _this.$options.lazyLoadSupportOption.reloadCallback(innerText);
                              _this.$options.selectList = matchedSelectList = data;
                              _this.setLazyLoadDataTotal(total);
                          }
                      }
                      _this.$matchedList = matchedSelectList;
                      if(!(!searchSupportOption.isViewItemsWhenNoInput && !innerText)) {
                          createSelectItems(_this, matchedSelectList);
                      }
                  }, 300);
              });
          }

          // input回车选中查询事件
          _this.$ele.on("keydown", function (e) {
              if (
                  e.keyCode === 13 &&
                  _this.$options.enterSelectSupportOption.support
              ) {
                  _this.$ele
                      .next()
                      .removeClass("icon-" + suffixIcon + "-top")
                      .addClass("icon-" + suffixIcon + "-bottom");
                  var innerText = _this.$options.searchSupportOption.support
                      ? _this.$ele.val().trim()
                      : "";
                  var dataId = "",
                      dataText = "";
                  var matchedSelectList = _this.$matchedList;
                  if (matchedSelectList.length > 0) {
                      // 优先显示选中项
                      var activeDom = _this.$ele.parent().find(".active");
                      if (activeDom.length > 0) {
                          var index = $(activeDom).attr("data-index");
                          dataId = matchedSelectList[index][_this.$options.keyField];
                          dataText = matchedSelectList[index][_this.$options.valueField];
                      } else {
                          // 区分enterSelectSupportOption.methods进行匹配
                          if (
                              _this.$options.enterSelectSupportOption.methods === "complete"
                          ) {
                              // 精准匹配,这里排除输入框空值的情况
                              var completeSelectList = innerText
                                  ? getCompleteSelectData(_this, matchedSelectList, innerText)
                                  : [];
                              if (completeSelectList.length > 0) {
                                  dataId = matchedSelectList[0][_this.$options.keyField];
                                  dataText = matchedSelectList[0][_this.$options.valueField];
                              }
                          } else {
                              // 部分匹配，这里需要考虑输入框空值情况下是否可以选中第一项的情况
                              if (
                                  innerText ||
                                  (!innerText && _this.$options.selectFirstWhenNoInput)
                              ) {
                                  dataId = matchedSelectList[0][_this.$options.keyField];
                                  dataText = matchedSelectList[0][_this.$options.valueField];
                              }
                          }
                      }
                  }
                  // 懒加载时，
                  if(_this.$options.lazyLoadSupportOption.support) {
                      _this.$options.selectList = _this.$matchedList = getMatchedSelectData(_this, _this.$matchedList, dataText);
                  }
                  _this.$curSelect = setSelectItemObj(_this, dataId, dataText);
                  _this.$ele.attr("data-select", dataId).val(dataText);
                  _this.$ele.parent().find(">ul").remove();
                  _this.closeFlag = false;
              } else if (e.keyCode === 40) {
                  // 向下按键
                  // 查找当前元素中是否存在active类
                  var activeDom = _this.$ele.parent().find(".active");
                  if (activeDom.length > 0) {
                      // 实现鼠标控制滚动条的变化
                      // 获取当前dom的高度
                      var offsetTop = $(activeDom)[0].offsetTop,
                          offsetHeight = $(activeDom)[0].offsetHeight;
                      // 获取ul的高度
                      var viewUlHeight = _this.$ele.parent().find("ul")[0].offsetHeight;
                      if (offsetTop + offsetHeight >= viewUlHeight) {
                          _this.$ele
                              .parent()
                              .find("ul")
                              .eq(0)
                              .scrollTop(
                                  offsetTop + offsetHeight - viewUlHeight + offsetHeight
                              );
                      }
                      var index = $(activeDom).attr("data-index");
                      if (+index < _this.$matchedList.length - 1) {
                          $(activeDom).removeClass("active").next().addClass("active");
                      }
                  } else {
                      _this.$ele.parent().find("li").eq(0).addClass("active");
                  }
              } else if (e.keyCode === 38) {
                  // 向上按键
                  var activeDom = _this.$ele.parent().find(".active");
                  if (activeDom.length > 0) {
                      var offsetTop = $(activeDom)[0].offsetTop,
                          offsetHeight = $(activeDom)[0].offsetHeight;
                      // 获取ul滚动的高度
                      var viewUlHeight = _this.$ele.parent().find("ul")[0].offsetHeight;
                      var viewUlScrollTop = _this.$ele
                          .parent()
                          .find("ul")
                          .eq(0)
                          .scrollTop();
                      if (
                          offsetTop + offsetHeight <= viewUlHeight + viewUlScrollTop &&
                          viewUlScrollTop > 0
                      ) {
                          _this.$ele
                              .parent()
                              .find("ul")
                              .eq(0)
                              .scrollTop(viewUlScrollTop - offsetHeight);
                      }
                      var index = $(activeDom).attr("data-index");
                      if (+index > 0) {
                          $(activeDom).removeClass("active").prev().addClass("active");
                      }
                  }
              }
          });
      },

      /**
       * 输入框change事件
       * @param {*} fn 回调函数
       */
      change: function (fn) {
          var _this = this;
          Object.defineProperty(_this, "$curSelect", {
              get() {
                  return _this.$curSelectReal;
              },
              set(newVal) {
                  _this.$curSelectReal = newVal;
                  if (newVal) {
                      fn(newVal, _this);
                  }
              },
          });
      },

      /**
       * 添加悬浮回调
       * @param fn
       */
      focus: function (fn) {
          var _this = this;
          _this.$ele.focus(function () {
              fn(_this);
          });
      },

      /**
       * 重新刷新数据方法
       * @param {*} selectList 下拉数组
       */
      refresh: function (selectList) {
          if (!selectList instanceof Array) {
              throw new Error(
                  "刷新下拉数据所调用的refresh方法传入非数组数据，请核实"
              );
          }
          var _this = this;
          if(!_this.$options.lazyLoadSupportOption.support) {
              var obj = { selectList: selectList };
              _this.defaultConfig = $.extend({}, _this.defaultConfig, obj);
              _this.$options = $.extend({}, _this.$options, obj);
          } else {
              _this.defaultConfig.selectList = [];
              _this.defaultConfig.lazyLoadSupportOption.total = 0;
              _this.$options.selectList = [];
              _this.$options.lazyLoadSupportOption.total = 0;
              _this.$matchedList = [];
          }

          _this.$ele
              .next()
              .removeClass("icon-" + _this.$options.suffixIcon + "-top")
              .addClass("icon-" + _this.$options.suffixIcon + "-bottom");
          _this.$ele.attr("data-select", "").val("");
          _this.$ele.parent().find(">ul").remove();
          _this.$curSelect = null;
      },

      /**
       * 获取key值方法
       * @returns
       */
      getKey: function () {
          var _this = this;
          return _this.$curSelect && _this.$curSelect[_this.$options.keyField] ? _this.$curSelect[_this.$options.keyField] : "";
      },

      /**
       * 获取key值方法
       * @returns
       */
      val: function () {
          var _this = this;
          return _this.$curSelect && _this.$curSelect[_this.$options.keyField] ? _this.$curSelect[_this.$options.keyField] : "";

      },

      /**
       * 获取value值方法
       * @returns
       */
      getValue: function () {
          var _this = this;
          return _this.$curSelect && _this.$curSelect[_this.$options.valueField] ? _this.$curSelect[_this.$options.valueField] : "";
      },

      /**
       * 赋值
       * @param {*} key
       */
      setKey: function(key) {
          var _this = this;
          var matchedArr = _this.$options.selectList.filter(
              (item) => item[_this.$options.keyField] === key
          );
          if (matchedArr.length === 0) {
              throw new Error("下拉选项赋值传入的key值不存在");
          }
          _this.$curSelect = matchedArr[0];
          _this.$ele
              .attr("data-select", matchedArr[0][_this.$options.keyField])
              .val(matchedArr[0][_this.$options.valueField]);
      },

      /**
       * 赋值
       * @param {*} key
       */
      setSelectItem: function (key) {
          var _this = this;
          var matchedArr = _this.$options.selectList.filter(
              (item) => item[_this.$options.keyField] === key
          );
          if (matchedArr.length === 0) {
              throw new Error("下拉选项赋值传入的key值不存在");
          }
          _this.$curSelect = matchedArr[0];
          _this.$ele
              .attr("data-select", matchedArr[0][_this.$options.keyField])
              .val(matchedArr[0][_this.$options.valueField]);
      },

      /**
       * 设置懒加载数量
       * @param {*} total
       */
      setLazyLoadDataTotal: function (total) {
          var _this = this;
          if (isNaN(+total)) {
              throw new Error("total只能输入数字格式")
          }
          _this.$options.lazyLoadSupportOption.total = total;
      },

      /**
       * 添加懒加载数据
       * @param {*} data
       */
      addLazyloadData: function (fn) {
          return new Promise(function (resolve, reject) {
              var data = fn();
              resolve(isArray(data, '懒加载数据'))
          })
      }
  };

  /**
   * 递归合并对象多层属性
   * @param {*} defaults
   * @param {*} options
   * @returns
   */
  function mergeOptions(defaults, options) {
      if (!options) {
          return defaults;
      }
      var result = JSON.parse(JSON.stringify(defaults));
      for (var key in options) {
          if (isObject(options[key])) {
              result[key] = mergeOptions(defaults[key], options[key]);
          } else {
              result[key] = options[key];
          }
      }
      return result;
  }

  /**
   * 判断是否包含某属性
   * @param {*} object
   * @param {*} propertyName
   * @returns
   */
  function hasProperty(object, propertyName) {
      return hasOwnProperty.call(object, propertyName);
  }

  /**
   * 判断是否是对象
   * @param {*} obj
   * @returns
   */
  function isObject(obj) {
      return Object.prototype.toString.call(obj) === "[object Object]";
  }

  /**
   * 判断是否为数组
   * @param {*} arr
   * @param {*} str
   * @returns
   */
  function isArray(arr, str) {
      if (Object.prototype.toString.call(arr) === "[object Array]") {
          return arr
      } else {
          throw new Error(`传入的 ${str} 数据格式必须设置为数组`)
      }
  }

  /**
   * 获取符合输入条件的数组数据
   * @param {*} _this
   * @param {*} selectList
   * @param {*} matchStr
   * @returns
   */
  function getMatchedSelectData(_this, selectList, matchStr) {
      let selectListCopy = Array.from(selectList);
      if (matchStr) {
          var matchedCondition =
                  _this.$options.searchSupportOption.matchedCondition,
              sensitive = _this.$options.searchSupportOption.sensitive;
          // 根据不同配置进行处理
          switch (matchedCondition) {
              case "all":
                  if (!sensitive) {
                      return selectListCopy.filter(
                          (item) =>
                              item[_this.$options.valueField]
                                  .toLowerCase()
                                  .indexOf(matchStr.toLowerCase()) > -1
                      );
                  } else {
                      return selectListCopy.filter(
                          (item) => item[_this.$options.valueField].indexOf(matchStr) > -1
                      );
                  }
                  break;
              case "start":
                  if (!sensitive) {
                      return selectListCopy.filter((item) =>
                          item[_this.$options.valueField]
                              .toLowerCase()
                              .startsWith(matchStr.toLowerCase())
                      );
                  } else {
                      return selectListCopy.filter((item) =>
                          item[_this.$options.valueField].startsWith(matchStr)
                      );
                  }
                  break;
              case "end":
                  if (!sensitive) {
                      return selectListCopy.filter((item) =>
                          item[_this.$options.valueField]
                              .toLowerCase()
                              .endsWith(matchStr.toLowerCase())
                      );
                  } else {
                      return selectListCopy.filter((item) =>
                          item[_this.$options.valueField].endsWith(matchStr)
                      );
                  }
                  break;
          }
      } else {
          return selectListCopy;
      }
  }

  /**
   * 动态生成下拉选项
   * @param {*} _this
   * @param {*} selectList
   * @param {*} curSelect
   */
  function createSelectItems(_this, selectList, curSelect) {
      // 测试 keyField, valueField是否准确
      var html = "<ul class='multiFunctionSelectList'>";
      html += getSelectListDomStr(_this, selectList, curSelect);
      html += "</ul>";
      _this.$ele.parent().append(html);
      // 当需要提供懒加载服务时绑定scroll事件
      if (_this.$options.lazyLoadSupportOption.support) {
          _this.bindScrollEvent(curSelect);
      }
  }

  /**
   * 添加或者删除遮罩层
   * @param {*} flag
   * @param {*} noList
   */
  function addorDeleteMaskLayer(_this, flag, noList) {
      if (flag) {
          var height = $("." + _this.$options.parentEleClass + " > .multiFunctionSelectList").height();
          var topHeight = $("." + _this.$options.parentEleClass + " > .multiFunctionSelectList >li")[0]
              .offsetHeight;
          var str = !noList
              ? "<div class='maskLayer'><i class='iconfont icon-loading loadingImg'></i></div>"
              : "<div class='maskLayer'>已加载最后一项</div>";
          var maskLayer = $(str);
          maskLayer.css({ height: height, top: topHeight });
          _this.$ele.parent().append(maskLayer);
      } else {
          _this.$ele.parent().find(".maskLayer").remove();
      }
  }

  /**
   * 精准匹配
   * @param {*} _this
   * @param {*} selectList
   * @param {*} matchStr
   * @returns
   */
  function getCompleteSelectData(_this, selectList, matchStr) {
      let selectListCopy = Array.from(selectList);
      if (_this.$options.sensitive) {
          return selectListCopy.filter(
              (item) =>
                  item[_this.$options.valueField].toLowerCase() ===
                  matchStr.toLowerCase()
          );
      } else {
          return selectListCopy.filter(
              (item) => item[_this.$options.valueField] === matchStr
          );
      }
  }

  /**
   * 获取下拉选项拼接dom字符串
   * @param {*} _this
   * @param {*} selectList
   * @param {*} curSelect
   * @param {*} curPageNum
   * @returns
   */
  function getSelectListDomStr(_this, selectList, curSelect, curPageSize = 0) {
      var html = "";
      if (selectList.length > 0) {
          var dataId = curSelect && curSelect.dataId ? curSelect.dataId : "";
          selectList.forEach(function (item, index) {
              html +=
                  "<li data-id='" +
                  item[_this.$options.keyField] +
                  "' data-index='" +
                  Number(curPageSize + index) +
                  "' title='" +
                  item[_this.$options.valueField] +
                  "' >" +
                  item[_this.$options.valueField] +
                  "</li>";
          });
      } else {
          html += "<li data-id='none'>暂无数据</li>";
      }
      return html;
  }

  /**
   * 设置下拉属性对象
   * @param {*} _this
   * @param {*} dataId key值
   * @param {*} dataText value值
   * @returns
   */
  function setSelectItemObj(_this, dataId, dataText) {
      var obj = Object.create(null);
      obj[_this.$options.keyField] = dataId;
      obj[_this.$options.valueField] = dataText;
      return obj;
  }

  /**
   * 动态生成后缀图标
   * @param {*} _this
   * @param {*} suffixIcon 图标
   */
  function createIconView(_this, suffixIcon) {
      var iconHtml = "<i class='iconfont icon-" + suffixIcon + "-bottom'></i>";
      _this.$ele.after(iconHtml);
  }

  $.fn.MultiFunctionSelect = function (opts) {
      return new MultiFunctionSelect(this, opts).init();
  };
