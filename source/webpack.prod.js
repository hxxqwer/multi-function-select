const path = require("path");
const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.config")

const prodConfig = {
    mode: 'production',
    devtool: 'source-map',
    output: {
        filename: "[name].min.js",
        path: path.resolve(__dirname, "dist"),
    },
}
module.exports = merge(commonConfig, prodConfig);