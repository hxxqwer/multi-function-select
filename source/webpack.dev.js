const path = require("path");
const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.config")

const devConfig = {
    mode: 'development',
    devtool: 'inline-source-ma',
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist"),
    },
}

module.exports = merge(commonConfig, devConfig)